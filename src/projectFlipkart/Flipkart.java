package projectFlipkart;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import java.time.Duration;
import java.util.Set;

public class Flipkart {
    public static void main(String[] args) {

        try {
            System.setProperty("webdriver.chrome.driver", "/home/vansh/Desktop/java/seleniumProjects/chromedriver");
            WebDriver driver = new ChromeDriver();
            driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
            driver.manage().window().maximize();
            driver.get("https://www.flipkart.com");
            driver.findElement(By.xpath("/html/body/div[2]/div/div/button")).click();
            driver.findElement(By.className("_3704LK")).sendKeys("POCO M4");
            driver.findElement(By.className("L0Z3Pu")).click();
            driver.findElement(By.xpath("//*[@id=\"container\"]/div/div[3]/div[1]/div[2]/div[2]/div/div/div/a/div[1]/div[1]/div/div/img")).click();
            String parentHandle= driver.getWindowHandle();
            System.err.println(parentHandle);
            Set<String> handles=driver.getWindowHandles();
            for(String handle:handles){
                if(!handle.equals(parentHandle)) {
                    driver.switchTo().window(handle);
                    driver.findElement(By.xpath("//*[@id=\"container\"]/div/div[3]/div[1]/div[1]/div[2]/div/ul/li[1]/button")).click();
                    WebElement placeOrder = driver.findElement(By.xpath("//button[contains(@class,'AWRsL')]/span[text()='Place Order']"));
                    placeOrder.click();
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
//for switch any tab to anyone
//                List<String> handleList = new ArrayList<String>(handles);
//                for(int i=0;i<handleList.size();i++){
//                    driver.switchTo().window(handleList.get(3));
//                }
//   driver.findElement(By.xpath("//body/div[@id='container']/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/form[1]/button[1]")).click();
//                        WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(10));
//  wait.until(ExpectedConditions.visibilityOf(placeOrder));