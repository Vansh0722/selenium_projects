package projectMeesho;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import java.time.Duration;

public class Meesho {
        WebDriver driver;
        WebElement searchProduct;
        WebElement selectProduct;
        WebElement proceedToCart;
        WebElement addProductToCart;
        WebElement phoneNumber;
        WebElement otp;

        public void invokeBrowser(){
            try{
                System.setProperty("webdriver.chrome.driver", "/home/vansh/Desktop/java/seleniumProjects/chromedriver");
                driver = new ChromeDriver();
                driver.manage().window().maximize();
                driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(20));
                driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(20));
                driver.get("https://meesho.com/");
                addProductToCart();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        public void addProductToCart(){
            try{
                driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
                searchProduct=driver.findElement(By.xpath("//span[contains(text(),'Jewellery &')]"));
                Actions action=new Actions(driver);
                action.moveToElement(searchProduct).perform();
                selectProduct= driver.findElement(By.xpath("//p[contains(text(), 'Rings')]"));
                selectProduct.click();
                Actions actions=new Actions(driver);
                actions.sendKeys(Keys.PAGE_DOWN).build().perform();
                proceedToCart =driver.findElement(By.xpath(" //a[@href='/elite-graceful-rings/p/1gfzwd']"));
                proceedToCart.click();
                addProductToCart=driver.findElement(By.xpath("//span[contains(text(), 'Add to Cart')]"));
                addProductToCart.click();
                phoneNumber =driver.findElement(By.xpath("//input[@type='tel']"));
                phoneNumber.sendKeys("9058885506");
                otp=driver.findElement(By.xpath("//span[text()=\"Send OTP\"]"));
                otp.click();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        public static void main(String[] args) {

            Meesho webPage=new Meesho();
            webPage.invokeBrowser();
        }
}






//git remote add origin git@gitlab.com:Vansh0722/selenium_projects.git