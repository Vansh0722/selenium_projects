package projectAmazon;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import java.time.Duration;
import java.util.Set;

public class Amazon {
    public static void main(String[] args) {
        try{
            System.setProperty("webdriver.chrome.driver", "/home/vansh/Desktop/java/seleniumProjects/chromedriver");
            WebDriver driver = new ChromeDriver();
            driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
            driver.manage().window().maximize();
            driver.get("https://www.amazon.in/");
            driver.findElement(By.id("twotabsearchtextbox")).sendKeys("Books");
            driver.findElement(By.id("nav-search-submit-button")).click();
            driver.findElement(By.xpath("//body/div[@id='a-page']/div[@id='search']/div[1]/div[1]/div[1]/span[3]/div[2]/div[3]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/h2[1]/a[1]/span[1]")).click();
            String parentHandle= driver.getWindowHandle();
            Set<String> childhandles=driver.getWindowHandles();
            for(String handle:childhandles) {
                if (!handle.equals(parentHandle)) {
                    driver.switchTo().window(handle);
                    driver.findElement(By.xpath("//input[@id='add-to-cart-button']")).click();
                    WebElement placeOrder =driver.findElement(By.xpath(
                            "//input[@name='proceedToRetailCheckout']"));
                    placeOrder.click();
                    WebElement enterNUmber = driver.findElement(By.xpath("//input[@id='ap_email']"));
                    enterNUmber.sendKeys("9058885506");
                    WebElement clickNumber = driver.findElement(By.xpath("//input[@id='continue']"));
                    clickNumber.click();
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
//this is for Wait for any element
//                    WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
//                    wait.until(ExpectedConditions.visibilityOf(driver.findElement(
//                            By.xpath("//body/div[@id='a-page']/div[@id='sw-full-view-container']/div[@id='sw-full-view']/div[@id='sw-atc-confirmation']/div[@id='sw-atc-actions']/div[@id='sw-atc-fst-buybox']/div[@id='sw-atc-bb']/div[@id='sw-atc-actions-buy-box-sign-in']/div[@id='sw-atc-buy-box']/form[@id='sw-ptc-form']/span[@id='sc-buy-box-ptc-button']/span[1]/input[1]"))));